Run docker-compose up -d

Run docker exec -it hw20 php index.php to insert data

full backup:

run docker exec hw20 mysqldump -uroot -proot123 --all-databases --source-data --single-transaction > ./backup/full.sql   

Time 29s
Size 23mb

Recover: 

run docker exec hw20 mysqldump -uadmin -p12345 mysql -uadmin -p12345 < ./backup/full.sql

time 7s

Differential backup:

docker run --rm --name percona-xtrabackup --network  homework20 --volumes-from hw20 percona/percona-xtrabackup \
xtrabackup --backup --datadir=/var/lib/mysql --target-dir=/backup/base --user=admin --password=12345

Time 12s Size 103M

Incremental backup:

docker run --rm --name percona-xtrabackup --network homework20 --volumes-from hw20 percona/percona-xtrabackup \
xtrabackup --backup --target-dir=/backup/inc1 --incremental-basedir=/backup/base --user=admin --password=12345

Time 5s
Size9M

Recover:

docker run --rm --name percona-xtrabackup --network homework20 --volumes-from hw20 percona/percona-xtrabackup \
xtrabackup --prepare --apply-log-only --target-dir=/backup/base --user=admin --password=12345

time: 5s

First incremental dump:
docker run --rm --name percona-xtrabackup --network homework20 --volumes-from hw20 percona/percona-xtrabackup \
xtrabackup --prepare --apply-log-only --target-dir=/backup/base \
--incremental-dir=/backup/inc1 --user=admin --password=12345
time: 19s

Second incremental dump:
docker run --rm --name percona-xtrabackup --network l20_p20 --volumes-from hw20 percona/percona-xtrabackup \
xtrabackup --prepare --target-dir=/backup/base \
--incremental-dir=/backup/inc2 --user=admin --password=12345
time: 10s







