<?php
declare(strict_types=1);
require_once 'vendor/autoload.php';

const CREATE_TABLE = true;
const INSERT_USERS = true;
const USERS_NUMBER_TO_INSERT = 10**4;

/** Script entrypoint */
execute();

/**
 * @return void
 */
function execute()
{
    $start = microtime(true);

    $mysqli = new mysqli('hw20', 'admin', '12345', 'homework20');

    if(CREATE_TABLE === true){
        createTable($mysqli);
    }

    if(INSERT_USERS === true){
        for ($i=0; $i < USERS_NUMBER_TO_INSERT; $i++){
            insertUserRow($mysqli, sampleUser());
        }
    }

    $mysqli->close();

    $delta = microtime(true) - $start;
    print_r($delta . "\n");
}

/**
 * @param $mysqli
 *
 * @return void
 */
function createTable($mysqli)
{
    $mysqli->query(
        "CREATE TABLE IF NOT EXISTS Users (
            id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name varchar(255),
            surname varchar(255),
            email varchar(255) NOT NULL,
            birthday date
        );
    ");
}

/**
 * @param $mysqli
 * @param $user
 *
 * @return void
 */
function insertUserRow($mysqli, $user)
{
    $mysqli->query(sprintf("
        INSERT INTO Users (name, surname, email, birthday)
        VALUES ('%s', '%s', '%s', '%s', '%s')",
        mysqli_real_escape_string($mysqli, $user['name']),
        mysqli_real_escape_string($mysqli, $user['surname']),
        mysqli_real_escape_string($mysqli, $user['email']),
        mysqli_real_escape_string($mysqli, $user['birthday'])
    ));
}

/**
 * @return array
 */
function sampleUser()
{
    /** @var Faker\Generator $faker */
    $faker = Faker\Factory::create();

    return [
        'name' => $faker->firstname(),
        'surname' => $faker->lastname(),
        'email' => $faker->email(),
        'birthday' => $faker->date('Y-m-d'),
    ];
}